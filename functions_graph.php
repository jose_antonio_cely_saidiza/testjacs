<?php

// ===============================================================================



function graphic_error () {
	global $config;

	Header('Content-type: image/png');
	$imgPng = imageCreateFromPng($config["homedir"].'/images/error.png');
	imageAlphaBlending($imgPng, true);
	imageSaveAlpha($imgPng, true);
	imagePng($imgPng);
}

// ***************************************************************************
// Draw a dynamic progress bar using GDlib directly
// ***************************************************************************

function progress_bar ($progress, $width, $height) {
	// Copied from the PHP manual:
	// http://us3.php.net/manual/en/function.imagefilledrectangle.php
	// With some adds from sdonie at lgc dot com
	// Get from official documentation PHP.net website. Thanks guys :-)
	function drawRating($rating, $width, $height) {
		
		if ($width == 0) {
			$width = 150;
		}
		if ($height == 0) {
			$height = 20;
		}

		$config['fontpath']="fonts/DejaVuSansCondensed.ttf";
		//$rating = $_GET['rating'];
		$ratingbar = (($rating/100)*$width)-2;

		$image = imagecreate($width,$height);
		

		//colors
		$back = ImageColorAllocate($image,255,255,255);
		imagecolortransparent ($image, $back);
		$border = ImageColorAllocate($image,174,174,174);
		$text = ImageColorAllocate($image,74,74,74);
		$red = ImageColorAllocate($image,255,60,75);
		$green = ImageColorAllocate($image,50,205,50);
		$fill = ImageColorAllocate($image,44,81,120);

		ImageFilledRectangle($image,0,0,$width-1,$height-1,$back);
		if ($rating > 100)
			ImageFilledRectangle($image,1,1,$ratingbar,$height-1,$red);
		elseif ($rating == 100)
			ImageFilledRectangle($image,1,1,$ratingbar,$height-1,$green);
		else
			ImageFilledRectangle($image,1,1,$ratingbar,$height-1,$fill);
			
		ImageRectangle($image,0,0,$width-1,$height-1,$border);
		if ($rating > 50)
			if ($rating > 100)
				ImageTTFText($image, 8, 0, ($width/4), ($height/2)+($height/5), $back, $config["fontpath"],__('Out of limits'));
			else
				ImageTTFText($image, 8, 0, ($width/2)-($width/10), ($height/2)+($height/5), $back, $config["fontpath"], $rating."%");
		else
			ImageTTFText($image, 8, 0, ($width/2)-($width/10), ($height/2)+($height/5), $text, $config["fontpath"], $rating."%");
		imagePNG($image);
		imagedestroy($image);
   	}

   	Header("Content-type: image/png");
	if ($progress > 100 || $progress < 0) {
		// HACK: This report a static image... will increase render in about 200% :-) useful for
		// high number of realtime statusbar images creation (in main all agents view, for example
		$imgPng = imageCreateFromPng("images/outlimits.png");
		imageAlphaBlending($imgPng, true);
		imageSaveAlpha($imgPng, true);
		imagePng($imgPng); 
   	} else 
   		drawRating($progress,$width,$height);
}

function generic_histogram ($width, $height, $mode, $valuea, $valueb, $maxvalue, $labela, $labelb) {
		
	$config['fontpath']="fonts/DejaVuSansCondensed.ttf";
	
	// $ratingA, $ratingB, $ratingA_leg, $ratingB_leg;
	$ratingA=$valuea;
	$ratingB=$valueb;
	
   	Header("Content-type: image/png");
	$image = imagecreate($width,$height);
	$white = ImageColorAllocate($image,255,255,255);
	imagecolortransparent ($image, $white);

	$black = ImageColorAllocate($image,0,0,0);
	$red = ImageColorAllocate($image,255,60,75);
	$blue = ImageColorAllocate($image,75,60,255);
	$grey = ImageColorAllocate($image,120,120,120);

	$margin_up = 2;
	$max_value = $maxvalue;
	if ($mode != 2) {
		$size_per = ($max_value / ($width-40));
	} else {
		$size_per = ($max_value / ($width));
	}
	if ($mode == 0) // with strips 
		$rectangle_height = ($height - 10 - 2 - $margin_up ) / 2;
	else
		$rectangle_height = ($height - 2 - $margin_up ) / 2;

	// First rectangle
	if ($size_per == 0)
		$size_per = 1;
	if ($mode != 2) {

		ImageFilledRectangle($image, 40, $margin_up, ($ratingA/$size_per)+40, $margin_up+$rectangle_height -1 , $blue);
		$legend = $ratingA;
		ImageTTFText($image, 7, 0, 0, $margin_up+8, $black, $config["fontpath"], $labela);
		// Second rectangle
		ImageFilledRectangle($image, 40, $margin_up+$rectangle_height + 1 , ($ratingB/$size_per)+40, ($rectangle_height*2)+$margin_up , $red);
		$legend = $ratingA;
		// ImageTTFText($image, 8, 0, ($width-10), ($height/2)+10, $black, $config_fontpath, $ratingB);
		ImageTTFText($image, 7, 0, 0,  $margin_up+$rectangle_height+8, $black, $config["fontpath"], $labelb);
	} else { // mode 2, without labels
		ImageFilledRectangle($image, 1, $margin_up, ($ratingA/$size_per)+1, $margin_up+$rectangle_height -1 , $blue);
		$legend = $ratingA;
		// Second rectangle
		ImageFilledRectangle($image, 1, $margin_up+$rectangle_height + 1 , ($ratingB/$size_per)+1, ($rectangle_height*2)+$margin_up , $red);
		$legend = $ratingA;

	}
	if ($mode == 0) { // With strips
		// Draw limits
		$risk_low =  ($config_risk_low / $size_per) + 40;
		$risk_med =  ($config_risk_med / $size_per) + 40;
		$risk_high =  ($config_risk_high / $size_per) + 40;
		imageline($image, $risk_low, 0, $risk_low , $height, $grey);
		imageline($image, $risk_med , 0, $risk_med  , $height, $grey);
		imageline($image, $risk_high, 0, $risk_high , $height, $grey);
		ImageTTFText($image, 7, 0, $risk_low-20, $height, $grey, $config["fontpath"], "Low");
		ImageTTFText($image, 7, 0, $risk_med-20, $height, $grey, $config["fontpath"], "Med.");
		ImageTTFText($image, 7, 0, $risk_high-25, $height, $grey, $config["fontpath"], "High");
	}
	imagePNG($image);
	imagedestroy($image);
}

// ===============================================================================
// Generic PIE graph
// ===============================================================================

function generic_pie_graph ($width=300, $height=200, $data, $legend) {
	
	error_reporting (0);
	if (sizeof($data) > 0) {
		// create the graph
		$driver=& Image_Canvas::factory('png',array('width'=>$width,'height'=>$height,'antialias' => 'native'));
		$Graph = & Image_Graph::factory('graph', $driver);
		// add a TrueType font
		$Font =& $Graph->addNew('font', $config["fontpath"]);
		// set the font size to 7 pixels
		$Font->setSize(7);
		$Graph->setFont($Font);
		// create the plotarea
		$Graph->add(
			Image_Graph::horizontal(
				$Plotarea = Image_Graph::factory('plotarea'),
				$Legend = Image_Graph::factory('legend'),
			50
			)
		);
		$Legend->setPlotarea($Plotarea);
		// Create the dataset
		// Merge data into a dataset object (sancho)
		$Dataset1 =& Image_Graph::factory('dataset');
		for ($a=0;$a < sizeof($data); $a++) {
			$Dataset1->addPoint(str_pad($legend[$a],15), $data[$a]);
		}
		$Plot =& $Plotarea->addNew('pie', $Dataset1);
		$Plotarea->hideAxis();
		// create a Y data value marker
		$Marker =& $Plot->addNew('Image_Graph_Marker_Value', IMAGE_GRAPH_PCT_Y_TOTAL);
		// create a pin-point marker type
		$PointingMarker =& $Plot->addNew('Image_Graph_Marker_Pointing_Angular', array(1, &$Marker));
		// and use the marker on the 1st plot
		$Plot->setMarker($PointingMarker);
		// format value marker labels as percentage values
		$Marker->setDataPreprocessor(Image_Graph::factory('Image_Graph_DataPreprocessor_Formatted', '%0.1f%%'));
		$Plot->Radius = 15;
		$FillArray =& Image_Graph::factory('Image_Graph_Fill_Array');
		$Plot->setFillStyle($FillArray);
		
		$FillArray->addColor('green@0.7');
		$FillArray->addColor('yellow@0.7');
		$FillArray->addColor('red@0.7');
		$FillArray->addColor('orange@0.7');
		$FillArray->addColor('blue@0.7');
		$FillArray->addColor('purple@0.7');
		$FillArray->addColor('lightgreen@0.7');
		$FillArray->addColor('lightblue@0.7');
		$FillArray->addColor('lightred@0.7');
		$FillArray->addColor('grey@0.6', 'rest');
		$Plot->explode(6);
		$Plot->setStartingAngle(0);
		// output the Graph
		$Graph->done();
	} else 
		graphic_error ();
}


function odo_generic ($value, $min, $max, $showvalue = 1) {
	require("dialgauge/phpdial_gauge.php");
	
	$my_gauge = new dial_gauge($value,$showvalue,$min,$max);
	header("Content-Type: image/png");	// png output
	$my_gauge->display_png();
}

// ****************************************************************************
//   MAIN Code
//   parse get parameters
// ****************************************************************************

if (isset($_GET["id_group"]))
	$id_group = $_GET["id_group"];
else
	$id_group = 0;
if (isset($_GET["width"]))
	$width= $_GET["width"];
else 
	$width= 280;
if (isset($_GET["height"]))
	$height= $_GET["height"];
else
	$height= 50;

if (isset($_GET["min"]))
	$min= $_GET["min"];
else
	$min= 0;
if (isset($_GET["max"]))
	$max= $_GET["max"];
else
	$max= 100;
if (isset($_GET["value"]))
	$value= $_GET["value"];
else
	$value= 0;
	
if (isset($_GET["percent"]))
	$percent= $_GET["percent"];
else
	$percent= 0;
if (isset($_GET["showvalue"]))
	$showvalue= $_GET["showvalue"];
else
	$showvalue= 1;		
if (isset($_GET["type"]))
	$type= $_GET["type"];
else
	$type= 0;
	
	
	$pure = 1;
	
	$value1 = 1;
	$value2 = 100; 
	$value3 = 50;
	// $value1, $value2, $value3  odo_generic ($value1, $value2, $value3, $width= 350, $height= 260, $max=100, $pure = 1) {
if ( $type == "progress")
	progress_bar ($percent, $width, $height);
elseif ($type == "odo")
	odo_generic ($value, $min, $max, $showvalue);
elseif ($type == "histogram")
	generic_histogram ($width, $height, $mode, $valuea, $valueb, $max, $labela, $labelb);	
elseif ($type == "pipe") {
	$data = (string) get_parameter ("data");
	$legend = (string) get_parameter ("legend");
	$data = split (',', $data);
	$legend = split (',', $legend);
	generic_pie_graph ($width, $height, $data, $legend);
}
?>
