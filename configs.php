<?php
/* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Library General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*  Copyright  2013  TECSUA SAS.
*  Email jose.cely@tecsua.com
*  Bogota Colombia
****************************************************************************/

$DEBUG = true;

$prefix = 'tecsua';
$thumbsize = 100;
// path a los archivos de log
$pathlogs = '/usr/local/nagios/var/archives/';

// $nagiosservices = array ('C:\ Drive Space', 'CPU Load', 'Memory Usage');
$nagiosservices = array ('66', '77', '25', '26', '5', '3', '4');
$nagcolor = array();
$nagcolor['OK'] = "#4e9a06";
$nagcolor['WARNING'] = "#edd400";
$nagcolor['CRITICAL'] = "#ef2929";
$nagcolor['UNKNOWN'] = "#777777";

// funciones
include_once "library/functions.php";

// para formumarios
include_once "library/classmakeform.php";

// para reportes
include_once "library/tecsuareports.php";

?>
