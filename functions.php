<?php
/* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Library General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*  Copyright  2013  TECSUA SAS.
*  Email jose.cely@tecsua.com
*  Bogota Colombia  
****************************************************************************/

/**
* Función para hacer une encabezado genérico
* @author Jose Antonio Cely Saidiza <jose.cely@gmail.com>
*/
function doheader($all = true) {
	
	header("Content-Type: text/html; charset=utf-8");
	header('Cache-control: private, no-cache, must-revalidate');
	header('Expires: 0');	

	if (isset($_REQUEST[TAction])) {  // TAction
		$_SESSION['TAction']=$_REQUEST[TAction];
	}

    global $db, $globalvar, $globalvar2, $appname;
   
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="codebase/dhtmlxchart.css"/>
	<script src="codebase/dhtmlxchart.js"></script>
    <title><?php echo $appname?></title>
	<link rel="stylesheet" type="text/css" href="style.css" />
    <?php 
	// inclusión de javascript en demanda
	require "javascript.php"; 
	?>	
</head>
<body>
    <div id="wrapper">
        <div id="headerwrap">
        <div id="header">
            <p>Nagios Tecsua reports</p>
        </div>
        </div>
<?php

}

function domenus() {	
        global $datetime, $db;

	$same = $_SERVER['PHP_SELF'];
	$act = $_SESSION['TAction'];
?>		
        <div id="navigationwrap">
		<ul class="menu">
		  <li><a href="<?php echo $same; ?>?TAction=&" <?php if ($act == '') {echo 'class="active"';}?>><span>Inicio</span></a></li>
		  <li><a href="<?php echo $same; ?>?TAction=host&" <?php if ($act == 'host') {echo 'class="active"';}?>><span>Hosts</span></a></li>
		  <li><a href="<?php echo $same; ?>?TAction=services&" <?php if ($act == 'services') {echo 'class="active"';}?>><span>Servicios</span></a></li>
		  <li><a href="<?php echo $same; ?>?TAction=reporte&" <?php if ($act == 'reporte') {echo 'class="active"';}?>><span>Reporte</span></a></li>
		  <li><a href="<?php echo $same; ?>?TAction=importar&" <?php if ($act == 'importar') {echo 'class="active"';}?>><span>Importar</span></a></li>
		  <li><a href="<?php echo $same; ?>?TAction=soporte&" <?php if ($act == 'soporte') {echo 'class="active"';}?>><span>Soporte</span></a></li>
		</ul>
        </div>
        <div id="contentwrap">
        <div id="content">
<?php

}


function dofother($bar = TRUE) {	
        global $datetime, $db;
	?>
        </div>
        </div>
        <div id="footerwrap">
        <div id="footer">
            <p>&copy; 2015. <a href="http://www.tecsua.com" target="_BLANK">TECSUA SAS</a>, Bogotá Colombia - info@tecsua.com</p></p>
        </div>
        </div>
    </div>
</body>
</html>
	<?php

}

function dodefault(){
    global $db, $titulo, $defaultcontent;
           

}

/**
    * Remove a value from a array
    * @param string $val
    * @param array $arr
    * @return array $array_remval
    */
    function array_remval($val, &$arr) {
          $array_remval = $arr;
          for($x=0;$x<count($array_remval);$x++)
          {
              $i=array_search($val,$array_remval);
              if (is_numeric($i)) {
                  $array_temp  = array_slice($array_remval, 0, $i );
                $array_temp2 = array_slice($array_remval, $i+1, count($array_remval)-1 );
                $array_remval = array_merge($array_temp, $array_temp2);
              }
          }
          return $array_remval;
    }

/**
 * function wordCut($sText, $iMaxLength, $sMessage)
 *
 * + cuts an wordt after $iMaxLength characters
 *
 * @param  string   $sText       the text to cut
 * @param  integer  $iMaxLength  the text's maximum length
 * @param  string   $sMessage    piece of text which is added to the cut text, e.g. '...read more'
 *
 * @returns string
 **/    
function wordCut($sText, $iMaxLength, $sMessage)
{
   if (strlen($sText) > $iMaxLength){
           
       $sString = wordwrap($sText, ($iMaxLength-strlen($sMessage)), '[cut]', 1);
       $asExplodedString = explode('[cut]', $sString);
      
       $sCutText = $asExplodedString[0];
      
       $sReturn = $sCutText.$sMessage;
   } else {
       $sReturn = $sText;
   }
  
   return $sReturn;
}

/*
* Retorna array con infomacion de un Archivo
*
*/

function fm_get_info2($file, $change = true) {
    $textfiles = array('txt', '(s|p)?html?', 'css', 'jse?', 'php[0-9]*', 'pr?l', 'pm', 'cgi', 'inc', 'csv', 'py', 'asp');
    $imagefiles = array('gif', 'jpe?g', 'png', 'bmp', 'tiff?', 'pict?', 'ico');
    $archivefiles = array('zip', '(r|t|j)ar', 't?gz', 't?bz2?', 'arj', 'ace', 'lzh', 'lha', 'xxe', 'uue?', 'iso', 'cab', 'r[0-9]+');
    $exefiles = array('exe', 'com', 'pif', 'bat', 'scr');
    $acrobatfiles = array('pd(f|x)');
    $wordfiles = array('do(c|t)', 'do(c|t)html');
    $excelfiles = array('xl(s|t|w|v)', 'xl(s|t)html', 'slk');
    
    $info = false;

    $filename = basename($file);  // saco nombre base
    if($filename != '.' && $filename != '..') { // si no son los opuntos de la carpeta
        $info['name'] = $filename;
        $explodename = explode(".",$filename); 
        $ext = $explodename[1];
        $info['onlyname']=$explodename[0];
        $info['extension'] = $ext;
        if( $change ) { // si tiene que eliminar espacios y pasar minusculas
            $info['onlyname'] = str_replace(' ', '_', $info['onlyname']);
            $info['onlyname'] = strtolower($info['onlyname']);
        }
        $info['image'] = is_dir($file) ? 'dir' : '';
    } else {
        return false;    
    }
    
    if($info && !$info['image']) {
      if(fm_is_type2($ext, $textfiles)) $info['image'] = 'text';
      else if(fm_is_type2($ext, $imagefiles)) $info['image'] = 'image';
      else if(fm_is_type2($ext, $archivefiles)) $info['image'] = 'archive';
      else if(fm_is_type2($ext, $exefiles)) $info['image'] = 'exe';
      else if(fm_is_type2($ext, $acrobatfiles)) $info['image'] = 'acrobat';
      else if(fm_is_type2($ext, $wordfiles)) $info['image'] = 'word';
      else if(fm_is_type2($ext, $excelfiles)) $info['image'] = 'excel';
      else $info['image'] = 'file';
    }
    return $info;
}

function fm_is_type2($ext, $types) {
    while(list(,$val) = each($types)) {
      if(eregi($val, $ext)) return true;
    }
    return false;
}

function convertunixtime($fecha)
{
        $temporalfecha1 = explode('-', $fecha);
        $dia1 = $temporalfecha1[2];
        $mes1 = $temporalfecha1[1];
        $anyo1 = $temporalfecha1[0];
        $horasfec1 = mktime(0,0,0,$mes1,$dia1,$anyo1);
        return $horasfec1;
}

 // objeto para sumar fechas
class Fecha {
    var $fecha;

    function Fecha($a = 0, $m = 0, $d = 0, $h = 0, $mm = 0) {
        If ($a==0) $a = date("Y");
        If ($m==0) $m = date("m");
        if ($d==0) $d = date("d");
        if ($h==0) $h = date("G");
        if ($mm==0) $mm = date("i");
        $this -> fecha = date("Y-m-d-G-i", mktime($h,$mm,0,$m,$d,$a));
    }

    function SumaTiempo($a = 0, $m = 0, $d = 0, $h = 0, $mm = 0) {
        $array_date = explode("-", $this->fecha);
        $this->fecha = date("Y-m-d-G-i", mktime($h + $array_date[3], $mm + $array_date[4], 0, $array_date[1] + $m, $array_date[2] + $d, $array_date[0] + $a));
    }

    function getFecha() { return $this->fecha; }
}

function pickercolor() {
        global $db;

}

// Returns 'date' in the format 'dd/mm/yyyy'
function fix_date ($date, $default='') {
	$date_array = preg_split ('/[\-\s]/', $date);
	if (sizeof($date_array) < 3) {
		return false;
	}

	if ($default != '' && $date_array[0] == '0000') {
		return $default;
	}
		
	return sprintf ('%02d/%02d/%04d', $date_array[2], $date_array[1], $date_array[0]);
}

/**
* Clase que crea una copia de una imagen, de un tamaño distinto, a través de distintos métodos
* Ejemplo de uso:
* <code>
* $o=new ImageResize($imagen_origen);
* $o->resizeWidth(100);
* $o->save($imagen_destino);
* </code>
* TODO: 
* - Definir de manera automática el formato de salida.
* - Definir otros tipos de formato de entrada, aparte de gif, jpg y png
*/
class ImageResize {
    var $file_s = "";
    var $gd_s;
    var $gd_d;
    var $width_s;
    var $height_s;
    var $width_d;
    var $height_d;
    var $aCreateFunctions = array(
        IMAGETYPE_GIF=>'imagecreatefromgif',
        IMAGETYPE_JPG=>'imagecreatefromjpg',
        IMAGETYPE_JPEG=>'imagecreatefromjpeg',
        IMAGETYPE_PNG=>'imagecreatefrompng',
    );
    
    /**
    * @param    string  Nombre del archivo
    */
    function ImageResize($source) 
    {
        $this->file_s = $source;
        list($this->width_s, $this->height_s, $type, $attr) = getimagesize($source, $info2);
        $createFunc = $this->aCreateFunctions[$type];
        if($createFunc) {
            $this->gd_s = $createFunc($source);
        }
    }
    
    /**
    * Retorna el Width de la imagen
    * @param    int     ancho en pixel
    */
    function GetWidth() 
    {
        return $this->width_s;
    } 
    
    /**
    * Redimensiona la imagen de forma proporcional, a partir del ancho
    * @param    int     ancho en pixel
    */
    function resizeWidth($width_d) 
    {
        $height_d = floor(($width_d*$this->height_s) /$this->width_s);
        $this->resizeWidthHeight($width_d, $height_d);
    }
    
    /**
    * Redimensiona la imagen de forma proporcional, a partir del alto
    * @param    int     alto en pixel
    */
    function resizeHeight($height_d) 
    {
        $width_d = floor(($height_d*$this->width_s) /$this->height_s);
        $this->resizeWidthHeight($width_d, $height_d);
    }
    
    /**
    * Redimensiona la imagen de forma proporcional, a partir del porcentaje del área
    * @param    int     porcentaje de área
    */
    function resizeArea($perc) 
    {
        $factor = sqrt($perc/100);
        $this->resizeWidthHeight($this->width_s*$factor, $this->height_s*$factor);
    }
    
    /**
    * Redimensiona la imagen, a partir de un ancho y alto determinado
    * @param    int     porcentaje de área
    */
    function resizeWidthHeight($width_d, $height_d) 
    {
        $this->gd_d = imagecreatetruecolor($width_d, $height_d);
        // desactivo el procesamiento automatico de alpha
        imagealphablending($this->gd_d, false);
        // hago que el alpha original se grabe en el archivo destino
        imagesavealpha($this->gd_d, true);
        imagecopyresampled($this->gd_d, $this->gd_s, 0, 0, 0, 0, $width_d, $height_d, $this->width_s, $this->height_s);
    }
    
    /**
    * Graba la imagen a un archivo de destino
    * @param    string  Nombre del archivo de salida
    */
    function save($file_d) 
    {
        imagepng($this->gd_d, $file_d);
        imagedestroy($this->gd_d);
    }
}


//  
/*
  convertir_a_letras($numero)
  function num2letras ()
  abstract Dado un n?mero lo devuelve escrito.
  param $num number - N?mero a convertir.
  param $fem bool - Forma femenina (true) o no (false).
  param $dec bool - Con decimales (true) o no (false).
  result string - Devuelve el n?mero escrito en letra.

*/ 

function convertir_a_letras($num, $fem = false, $dec = true) {
//if (strlen($num) > 14) die("El n?mero introducido es demasiado grande");
   $matuni[2]  = "DOS";
   $matuni[3]  = "TRES";
   $matuni[4]  = "CUATRO";
   $matuni[5]  = "CINCO";
   $matuni[6]  = "SEIS";
   $matuni[7]  = "SIETE";
   $matuni[8]  = "OCHO";
   $matuni[9]  = "NUEVE";
   $matuni[10] = "DIEZ";
   $matuni[11] = "ONCE";
   $matuni[12] = "DOCE";
   $matuni[13] = "TRECE";
   $matuni[14] = "CATORCE";
   $matuni[15] = "QUINCE";
   $matuni[16] = "DIECISEIS";
   $matuni[17] = "DIECISIETE";
   $matuni[18] = "DIECIOCHO";
   $matuni[19] = "DIECINUEVE";
   $matuni[20] = "VEINTE";
   $matunisub[2] = "DOS";
   $matunisub[3] = "TRES";
   $matunisub[4] = "CUATRO";
   $matunisub[5] = "QUIN";
   $matunisub[6] = "SEIS";
   $matunisub[7] = "SETE";
   $matunisub[8] = "OCHO";
   $matunisub[9] = "NOVE";

   $matdec[2] = "VEINT";
   $matdec[3] = "TREINTA";
   $matdec[4] = "CUARENTA";
   $matdec[5] = "CINCUENTA";
   $matdec[6] = "SESENTA";
   $matdec[7] = "SETENTA";
   $matdec[8] = "OCHENTA";
   $matdec[9] = "NOVENTA";
   $matsub[3]  = 'MILL';
   $matsub[5]  = 'BILL';
   $matsub[7]  = 'MILL';
   $matsub[9]  = 'TRILL';
   $matsub[11] = 'MILL';
   $matsub[13] = 'BILL';
   $matsub[15] = 'MILL';
   $matmil[4]  = 'MILLONES';
   $matmil[6]  = 'BILLONES';
   $matmil[7]  = 'DE BILLONES';
   $matmil[8]  = 'MILLONES DE BILLONES';
   $matmil[10] = 'TRILLONES';
   $matmil[11] = 'DE TRILLONES';
   $matmil[12] = 'MILLONES DE TRILLONES';
   $matmil[13] = 'DE TRILLONES';
   $matmil[14] = 'BILLONES DE TRILLONES';
   $matmil[15] = 'DE BILLONES DE TRILLONES';
   $matmil[16] = 'MILLONES DE BILLONES DE TRILLONES';

   $num = trim((string)@$num);
   if ($num[0] == '-') {
      $neg = 'MENOS ';
      $num = substr($num, 1);
   }else
      $neg = '';
   while ($num[0] == '0') $num = substr($num, 1);
   if ($num[0] < '1' or $num[0] > 9) $num = '0' . $num;
   $zeros = true;
   $punt = false;
   $ent = '';
   $fra = '';
   for ($c = 0; $c < strlen($num); $c++) {
      $n = $num[$c];
      if (! (strpos(".,'''", $n) === false)) {
         if ($punt) break;
         else{
            $punt = true;
            continue;
         }

      }elseif (! (strpos('0123456789', $n) === false)) {
         if ($punt) {
            if ($n != '0') $zeros = false;
            $fra .= $n;
         }else

            $ent .= $n;
      }else

         break;

   }
   $ent = '     ' . $ent;
   if ($dec and $fra and ! $zeros) {
      $fin = ' COMA';
      for ($n = 0; $n < strlen($fra); $n++) {
         if (($s = $fra[$n]) == '0')
            $fin .= ' CERO';
         elseif ($s == '1')
            $fin .= $fem ? ' UNA' : ' UN';
         else
            $fin .= ' ' . $matuni[$s];
      }
   }else
      $fin = '';
   if ((int)$ent === 0) return 'CERO ' . $fin;
   $tex = '';
   $sub = 0;
   $mils = 0;
   $neutro = false;
   while ( ($num = substr($ent, -3)) != '   ') {
      $ent = substr($ent, 0, -3);
      if (++$sub < 3 and $fem) {
         $matuni[1] = 'UNA';
         $subcent = 'AS';
      }else{
         $matuni[1] = $neutro ? 'UN' : 'UNO';
         $subcent = 'OS';
      }
      $t = '';
      $n2 = substr($num, 1);
      if ($n2 == '00') {
      }elseif ($n2 < 21)
         $t = ' ' . $matuni[(int)$n2];
      elseif ($n2 < 30) {
         $n3 = $num[2];
         if ($n3 != 0) $t = 'I' . $matuni[$n3];
         $n2 = $num[1];
         $t = ' ' . $matdec[$n2] . $t;
      }else{
         $n3 = $num[2];
         if ($n3 != 0) $t = ' Y ' . $matuni[$n3];
         $n2 = $num[1];
         $t = ' ' . $matdec[$n2] . $t;
      }
      $n = $num[0];
      if ($n == 1) {
         $t = ' CIENTO' . $t;
      }elseif ($n == 5){
         $t = ' ' . $matunisub[$n] . 'IENT' . $subcent . $t;
      }elseif ($n != 0){
         $t = ' ' . $matunisub[$n] . 'CIENT' . $subcent . $t;
      }
      if ($sub == 1) {
      }elseif (! isset($matsub[$sub])) {
         if ($num == 1) {
            $t = ' MIL';
         }elseif ($num > 1){
            $t .= ' MIL';
         }
      }elseif ($num == 1) {
         $t .= ' ' . $matsub[$sub] . '&Oacute;N';
      }elseif ($num > 1){
         $t .= ' ' . $matsub[$sub] . 'ONES';
      }   
      if ($num == '000') $mils ++;
      elseif ($mils != 0) {
         if (isset($matmil[$sub])) $t .= ' ' . $matmil[$sub];
         $mils = 0;
      }
      $neutro = true;
      $tex = $t . $tex;
   }
   $tex = $neg . substr($tex, 1) . $fin;
   return ucfirst($tex);
}
?>
