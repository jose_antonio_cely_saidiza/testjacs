<?php

/* This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *  Copyright  2009 Jose Antonio Cely Saidiza
 *  Email jose.cely@gmail.com
 *  Bogotá Colombia
 * ************************************************************************** */

/**
 * @author Osmar Ricardo Barrera Agudelo <osmar.barrera@tecsua.com>
 * @version 1
 * TODO: 
 */
class tecsuareport {

    /**
     * Retorna el javascript para el grafico de lineas
     * @param    string  $Variable variable
     * @param    string  $graphtype tipo de chart (bar,line,spline)
     */
    function MakeBasicChart($Variable, $jsondata, $estaciones, $captionbottom, $ytitle, $bounds = false, $graphtype = "line") {
        global $colors;

        $CantNombres = count($estaciones);
        $js .= 'var jsondata' . $Variable . ' =[' . $jsondata . '];';
        $js .= '
            var chart' . $Variable . ' = new dhtmlXChart({
            view: "' . $graphtype . '",
				container: "chart' . $Variable . '",
				value: "#' . str_replace(' ', '_', str_replace('.', '', $estaciones[0])) . '#",
				';
				
			if ($graphtype == "spline") {
				
			$js .= '
				item: {
                    borderColor: "' . $colors[0] . '", color: "#ffffff",
                },
                line: {
                    color: "' . $colors[0] . '", width: 3,
                },
                tooltip: {
                        template: "' . $estaciones[0] . ' - #' . str_replace(' ', '_', str_replace('.', '', $estaciones[0])) . '#",
                },
                offset: 0,';
              } else {
              	$js .= '
              	color: "' . $colors[0] . '",
              	alpha: 0.7,
                item:{
                    radius:0
                },
             	';
              }
              $js .= '
                xAxis: {
                        template: "#axis#", title: "' . $captionbottom . '"
                },
                yAxis: {
                ';
                if ($bounds) {
                	if ($bounds['max'] > $bounds['min']) {
                		$unit =  round(($bounds['max']/20), 1);
                		$start = $bounds['min'];
                		$end = $bounds['max'];
               $js .= '
						  start: '.$start.',
							end: '.$end.',
							step: Math.round('.$unit.'),
						';  	
                	}
                }
               $js .= '
                        title: "' . $ytitle . '",														
                },
                padding: {
                        left: 60, bottom: 50, top:15, right:15,
                },
                });
			';
        //iterar la cantidad de barras
        for ($c = 1; $c < $CantNombres; $c++) {
            $js .= 'chart' . $Variable . '.addSeries({
		            value: "#' . str_replace(' ', '_', str_replace('.', '', $estaciones[$c])) . '#",            
            ';
            	if ($graphtype == "spline") {
            		 $js .= '
		            item: {
		                    borderColor: "' . $colors[$c] . '", color: "#ffffff",
		            },
		            line: {
		                    color: "' . $colors[$c] . '", width: 3,
		            },
		            tooltip: {
		                    template: "' . $estaciones[$c] . '- #' . str_replace(' ', '_', str_replace('.', '', $estaciones[$c])) . '#",
		            }
		            ';
                } else {
                	$js .= '
                	 	color: "' . $colors[$c] . '",
                	 	alpha: 0.5,
                	 ';
                }
            $js .= '    						                    
            });';
        }
        $js .= 'chart' . $Variable . '.parse(jsondata' . $Variable . ',"json");	';
        return $js;
    }

    /**
     * Retorna el javascript para el grafico de radar
     * @param    string  $Variable variable
     */
    function MakeRadarChart($Variable, $jsondata, $estaciones, $captionbottom, $ytitle) {

        $colors[] = '#3399ff';
        $colors[] = '#66cc00';

        $CantNombres = count($estaciones);

        $js = '
            var jsondata' . $Variable . ' =[' . $jsondata . '];
            var chart' . $Variable . ' = new dhtmlXChart({
                view: "radar",
                container: "chart' . $Variable . '",
                value: "#' . $estaciones[0] . '#",
                tooltip: {
                        template: "' . $estaciones[0] . ' - #' . $estaciones[0] . '#",
                },
                color: "#3399ff",
                line: {
                        color: "#3399ff",
                        width: 1,
                },
                fill: "#3399ff",
                xAxis: {
                        template: "#axis#",
                },
                disableItems: true,
                alpha: 0.2,
                legend: {
                        layout: "y",
                        align: "right",
                        width: 140,
                        valign: "middle",
                        marker: {
                                width: 15,
                                radius: 3,
                        },					
                        values: [';
        $gc = 0;
        foreach ($estaciones as $estacionww) {
            if ($gc == 0) {
                $estacion = 'Velocidad del viento';
            } else {
                $estacion = ' Dirección del viento';
            }
            $js .= '
                        {
                            text: "' . $estacion . '",
                            color: "' . $colors[$gc] . '"
                        },';
            $gc++;
        }
        $js .= '        ],
                    }
                });';
        //iterar la cantidad de barras
        for ($c = 1; $c < $CantNombres; $c++) {
            $js .= 'chart' . $Variable . '.addSeries({
                        value: "#' . $estaciones[$c] . '#",
                        tooltip: {
                                template: "#' . $estaciones[$c] . '#",
                        },
                        fill: "' . $colors[$c] . '",
                        line: {
                                color: "' . $colors[$c] . '",
                                width: 1,
                        }										                    
                    });';
        }

        $js .= 'chart' . $Variable . '.parse(jsondata' . $Variable . ',"json");';

        return $js;
    }
    
 function MakePieChart($Variable, $jsondata) {
        global $colors;

		    $js .= '
			var jsondatapie' . $Variable . ' =[' . $jsondata . '];
			var piechart' . $Variable . '  =  new dhtmlXChart({
			view:"pie",
		    container: "piechart' . $Variable . '",
			value:"#percen#",
            labelOffset:-5,
            label:function(obj){
                var sum = piechart' . $Variable . '.sum("#percen#");
                var text = Math.round(parseFloat(obj.percen)/sum*100)+"%";
                return "<div class=\'label\' style=\'border:1px solid "+obj.color+"\'>"+text+"</div>";
            },
            color:"#color#",
            legend:{
                width: 75,
                align:"right",
                valign:"middle",
                template:"#warn#"
		    }
		});
		piechart' . $Variable . '.parse(jsondatapie' . $Variable . ',"json");';

        return $js; 
    }

    function MakeBarChart($Variable, $jsondata, $estaciones, $captionbottom, $ytitle, $graphtype = "bar") {
        global $colors;

        $CantNombres = count($estaciones);
        $js .= 'var jsondata' . $Variable . ' =[' . $jsondata . '];';
        $js .= '
            var chart' . $Variable . ' = new dhtmlXChart({
                view: "' . $graphtype . '",
		container: "chart' . $Variable . '",
		value: "#' . str_replace(' ', '_', str_replace('.', '', $estaciones[0])) . '#",
                color: "' . $colors[0] . '",
                gradient: "rising",
                width: 60,
                tooltip: {
                        template: "' . $estaciones[0] . ' - #' . str_replace(' ', '_', str_replace('.', '', $estaciones[0])) . '#",
                },
                xAxis: {
                        template: "#axis#", title: "' . $captionbottom . '"
                },
                yAxis: {
                        title: "' . $ytitle . '",														
                },

                legend: {
                        margin: 8,									
                        layout: "y",
                        align: "right",
                        width: 110,
                        valign: "middle",
                        values: [';
        $gc = 0;
        foreach ($estaciones as $estacion) {
            $js .= '
                            {
                                text: "' . $estacion . '",
                                color: "' . $colors[$gc] . '"
                            },';
            $gc++;
        }
        $js .= '        ],
                    }
                });
			';
        //iterar la cantidad de barras
        for ($c = 1; $c < $CantNombres; $c++) {
            $js .= 'chart' . $Variable . '.addSeries({
                value: "#' . str_replace(' ', '_', str_replace('.', '', $estaciones[$c])) . '#",
                color: "' . $colors[$c] . '",
                tooltip: {
                        template: "' . $estaciones[$c] . '- #' . str_replace(' ', '_', str_replace('.', '', $estaciones[$c])) . '#",
                }						                    
            });';
        }
        $js .= 'chart' . $Variable . '.parse(jsondata' . $Variable . ',"json");	';
        return $js;
    }

}
?>
